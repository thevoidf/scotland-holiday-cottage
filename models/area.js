module.exports = knex => {
	return {
		insertArea: (area, returning = '*') => {
			return knex
				.insert(area)
				.into('cottage_areas')
				.returning(returning);
		},
		findArea: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_areas')
				.where(where);
		},
		findOneArea: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_areas')
				.first();
		},
		findAllArea: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_areas');
		},
		updateArea: (area, where, returning = '*') => {
			return knex('cottage_areas')
				.where(where)
				.update(area)
				.returning(returning);
		},
		removeArea: where => {
			return knex('cottage_areas')
				.where(where)
				.returning(['id'])
				.del();
		},


		insertCounty: (area, returning = '*') => {
			return knex
				.insert(area)
				.into('cottage_counties')
				.returning(returning);
		},
		findCounty: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_counties')
				.where(where);
		},
		findOneCounty: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_counties')
				.first();
		},
		findAllCounty: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_counties');
		},
		updateCounty: (area, where, returning = '*') => {
			return knex('cottage_counties')
				.where(where)
				.update(area)
				.returning(returning);
		},
		removeCounty: where => {
			return knex('cottage_counties')
				.where(where)
				.returning(['id'])
				.del();
		},


		insertTown: (area, returning = '*') => {
			return knex
				.insert(area)
				.into('cottage_towns')
				.returning(returning);
		},
		findTown: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_towns')
				.where(where);
		},
		findOneTown: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_towns')
				.first();
		},
		findAllTown: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_towns');
		},
		updateTown: (area, where, returning = '*') => {
			return knex('cottage_towns')
				.where(where)
				.update(area)
				.returning(returning);
		},
		removeTown: where => {
			return knex('cottage_towns')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
