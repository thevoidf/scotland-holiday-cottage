module.exports = knex => {
	return {
		insert: (cottage, returning = '*') => {
			return knex
				.insert(cottage)
				.into('cottages')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottages')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottages')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('cottages');
		},
		update: (cottage, where, returning = '*') => {
			return knex('cottages')
				.where(where)
				.update(cottage)
				.returning(returning);
		},
		remove: where => {
			return knex('cottages')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
