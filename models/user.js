module.exports = knex => {
	return {
		insert: (user, returning = '*') => {
			return knex
				.insert(user)
				.into('users')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('users')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('users')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('users');
		},
		update: (where, user, returning = '*') => {
			return knex('users')
				.where(where)
				.update(user)
				.returning(returning);
		},
		remove: where => {
			return knex('users')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
