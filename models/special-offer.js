module.exports = knex => {
	return {
		insert: (specialOffer, returning = '*') => {
			return knex
				.insert(specialOffer)
				.into('special_offers')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('special_offers')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('special_offers')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('special_offers');
		},
		update: (where, specialOffer, returning = '*') => {
			return knex('special_offers')
				.where(where)
				.update(specialOffer)
				.returning(returning);
		},
		remove: where => {
			return knex('special_offers')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
