module.exports = knex => {
	return {
		insert: (navigation, returning = '*') => {
			return knex
				.insert(navigation)
				.into('navigations')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('navigations')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('navigations')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('navigations');
		},
		update: (navigation, where, returning = '*') => {
			return knex('navigations')
				.where(where)
				.update(navigation)
				.returning(returning);
		},
		remove: where => {
			return knex('navigations')
				.where(where)
				.returning(['id'])
				.del();
		},

		insertLink: (link, returning = '*') => {
			return knex
				.insert(link)
				.into('navigation_links')
				.returning(returning);
		},
		findLink: (where, select = '*') => {
			return knex
				.select(select)
				.from('navigation_links')
				.where(where);
		},
		findOneLink: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('navigation_links')
				.first();
		},
		findAllLink: (select = '*') => {
			return knex
				.select(select)
				.from('navigation_links');
		},
		updateLink: (link, where, returning = '*') => {
			return knex('navigation_links')
				.where(where)
				.update(link)
				.returning(returning);
		},
		removeLink: where => {
			return knex('navigation_links')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
