module.exports = knex => {
	const LinkBlock = require('./link-block')(knex);
	const Navigation = require('./navigation')(knex);

	return {
		insert: function(article, returning = '*') {
			return knex
				.insert(article)
				.into('articles')
				.returning(returning);
		},
		find: function(where, select = '*', eagerLoad) {
			const findArticles = knex
				.select(select)
				.from('articles')
				.where(where);

			if (eagerLoad) {
				return findArticles.then(articles => {
					return Promise.all(
						articles.map(article => this.articleEagerLoad(article))
					);
				})
			}

			return findArticles;
		},
		findOne: function(where, select = '*', eagerLoad = false) {
			const find = knex
				.select(select)
				.where(where)
				.from('articles')
				.first();

			if (eagerLoad) {
				return find.then(article => {
					if (!article)
						return Promise.reject('Article not found');
					return this.articleEagerLoad(article);
				});
			}
			return find;
		},
		findAll: function(select = '*', eagerLoad = false) {
			return this.find({}, select, eagerLoad);
		},
		update: function(article, where, returning = '*') {
			return knex('articles')
				.where(where)
				.update(article)
				.returning(returning);
		},
		remove: function(where) {
			return knex('articles')
				.where(where)
				.returning(['id'])
				.del();
		},

		articleEagerLoad: article => {
			return Promise.all([
				article,
				LinkBlock.find({ articleId: article.id })
			]).then(([article, linkBlocks]) => {
				article.linkBlocks = linkBlocks;
				return Promise.all([
					article,
					Navigation.find({ articleId: article.id })
				]);
			}).then(([article, navigations]) => {
				const promises = [ article, null, null ];
				article.navigations = {};
				navigations.forEach(nav => {
					if (nav.isTop) {
						promises[1] = Navigation.findLink({ navigationId: nav.id });
						article.navigations.top = nav;
					}
					if (!nav.isTop) {
						promises[2] = Navigation.findLink({ navigationId: nav.id });
						article.navigations.bottom = nav;
					}
				});
				return Promise.all(promises);
			}).then(([article, topNavigationLinks, bottomNavigationLinks]) => {
				if (Array.isArray(topNavigationLinks))
					article.navigations.top.links = topNavigationLinks;
				if (Array.isArray(bottomNavigationLinks))
					article.navigations.bottom.links = bottomNavigationLinks;
				return article;
			});
		}
	}
};
