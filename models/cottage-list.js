module.exports = knex => {
	return {
		insert: (cottageList, returning = '*') => {
			return knex
				.insert(cottageList)
				.into('cottage_lists')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_lists')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_lists')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_lists');
		},
		update: (cottageList, where, returning = '*') => {
			return knex('cottage_lists')
				.where(where)
				.update(cottageList)
				.returning(returning);
		},
		remove: where => {
			return knex('cottage_lists')
				.where(where)
				.returning(['id'])
				.del();
		},


		insertBlock: (cottageListBlock, returning = '*') => {
			return knex
				.insert(cottageListBlock)
				.into('cottage_list_link_blocks')
				.returning(returning);
		},
		findBlock: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_list_link_blocks')
				.where(where);
		},
		findOneBlock: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_list_link_blocks')
				.first();
		},
		findAllBlock: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_list_link_blocks');
		},
		updateBlock: (cottageListBlock, where, returning = '*') => {
			return knex('cottage_list_link_blocks')
				.where(where)
				.update(cottageListBlock)
				.returning(returning);
		},
		removeBlock: where => {
			return knex('cottage_list_link_blocks')
				.where(where)
				.returning(['id'])
				.del();
		},


		insertLink: (cottageListLink, returning = '*') => {
			return knex
				.insert(cottageListLink)
				.into('cottage_list_links')
				.returning(returning);
		},
		findLink: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_list_links')
				.where(where);
		},
		findOneLink: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_list_links')
				.first();
		},
		findAllLink: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_lists');
		},
		updateLink: (cottageListLink, where, returning = '*') => {
			return knex('cottage_list_links')
				.where(where)
				.update(cottageListLink)
				.returning(returning);
		},
		removeLink: where => {
			return knex('cottage_list_links')
				.where(where)
				.returning(['id'])
				.del();
		},

		insertCottage: (cottage, returning = '*') => {
			return knex
				.insert(cottage)
				.into('cottage_list_cottages')
				.returning(returning);
		},
		findCottage: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_list_cottages')
				.where(where);
		},
		findOneCottage: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_list_cottages')
				.first();
		},
		findAllCottage: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_list_cottages');
		},
		updateCottage: (cottage, where, returning = '*') => {
			return knex('cottage_list_cottages')
				.where(where)
				.update(cottage)
				.returning(returning);
		},
		removeCottage: where => {
			return knex('cottage_list_cottages')
				.where(where)
				.returning(['id'])
				.del();
		}

	}
};
