module.exports = knex => {
	return {
		insert: (filter, returning = '*') => {
			return knex
				.insert(filter)
				.into('filter_properties')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('filter_properties')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('filter_properties')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('filter_properties');
		},
		update: (where, filter, returning = '*') => {
			return knex('filter_properties')
				.where(where)
				.update(filter)
				.returning(returning);
		},
		remove: where => {
			return knex('filter_properties')
				.where(where)
				.returning(['id'])
				.del();
		},

		insertCottageFilter: (filter, returning = '*') => {
			return knex
				.insert(filter)
				.into('cottage_filter_properties')
				.returning(returning);
		},
		findCottageFilter: (where, select = '*') => {
			return knex
				.select(select)
				.from('cottage_filter_properties')
				.where(where);
		},
		findOneCottageFilter: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('cottage_filter_properties')
				.first();
		},
		findAllCottageFilter: (select = '*') => {
			return knex
				.select(select)
				.from('cottage_filter_properties');
		},
		updateCottageFilter: (where, filter, returning = '*') => {
			return knex('cottage_filter_properties')
				.where(where)
				.update(filter)
				.returning(returning);
		},
		removeCottageFilter: where => {
			return knex('cottage_filter_properties')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
