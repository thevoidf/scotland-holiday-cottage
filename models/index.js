const env = process.env.NODE_ENV || 'development';
const knexConfig = require('../config/database.js')[env];
const knexStringcse = require('knex-stringcase');
const knex = require('knex')(knexStringcse(knexConfig));
const fs = require('fs');
const { toPascalCase } = require('../utils');

fs.readdirSync(__dirname).forEach(file => {
	const model = file.replace(/\.js$/, '');
	if (model === 'index' || model.startsWith('.'))
		return;
	const modelName = toPascalCase(model).replace('-', '');
	module.exports[modelName] = require(`./${model}`)(knex);
});

module.exports.knex = knex;
