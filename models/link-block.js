const { knex } = require('../models');

module.exports = knex => {
	return {
		insert: (linkBlock, returning = '*') => {
			return knex
				.insert(linkBlock)
				.into('link_blocks')
				.returning(returning);
		},
		find: (where, select = '*') => {
			return knex
				.select(select)
				.from('link_blocks')
				.where(where);
		},
		findOne: (where, select = '*') => {
			return knex
				.select(select)
				.where(where)
				.from('link_blocks')
				.first();
		},
		findAll: (select = '*') => {
			return knex
				.select(select)
				.from('link_blocks');
		},
		update: (linkBlock, where, returning = '*') => {
			return knex('link_blocks')
				.where(where)
				.update(linkBlock)
				.returning(returning);
		},
		remove: where => {
			return knex('link_blocks')
				.where(where)
				.returning(['id'])
				.del();
		}
	}
};
