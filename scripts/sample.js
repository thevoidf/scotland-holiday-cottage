const { knex, User, Article, LinkBlock, Navigation, Cottage, Area } = require('../models');
const bcrypt = require('bcrypt');

const users = [
	{
		username: 'alex',
		email: 'alex@mail.com',
		isAdmin: true,
		password: 'apples'
	},
	{
		username: 'mike',
		email: 'mike@mail.com',
		password: 'apples'
	}
];

const cottages = [
	{
		// area: 'scotland',
		// county: 'scotland county',
		slug: 'scotland-cottage',
		listingTitle: 'title list',
		areaId: 1,
		countyId: 1,
		townId: 1
	}
];

const areas = [
	{
		name: 'area 51',
		counties: [
			{
				name: 'county 101',
				towns: [
					{ name: 'town 12' }
				]
			},
			{
				name: 'county 99',
				towns: [
					{ name: 'town 92' }
				]
			}
		]
	},
	{
		name: 'area woo',
		counties: [
			{
				name: 'county boo',
				towns: [
					{ name: 'town foo' }
				]
			},
			{
				name: 'county bar',
				towns: [
					{ name: 'town zobo' }
				]
			}
		]
	}
]

const articles = [
	{
		metaTitle: 'test meta',
		metaDescription: 'test desc',
		metaImage: 'image.png',
		metaRobots: 'this,that',
		mainTitle: 'test article',
		url: '/test/article',
		mainParagraph: '<p>main</p>',
		body: '<p>body</p>',
		userId: 1,
		linkBlocks: [
			{
				title: 'link block 1',
				linkTitle: 'link block 1 link title',
				linkHref: '/href/1'
			},
			{
				title: 'link block 2',
				linkTitle: 'link block 2 link title',
				linkHref: '/href/2'
			}
		],
		navigations: [
			{
				title: 'top nav',
				paragraph: 'top para',
				isTop: true,
				links: [ { title: 'top link title', href: '/top/href' } ]
			},
			{
				title: 'bottom nav',
				paragraph: 'bottom para',
				isTop: false,
				links: [ { title: 'bottom link title', href: '/bottom/href' } ]
			}
		]
	},
	{
		metaTitle: 'test mike',
		metaDescription: 'test desc',
		metaImage: 'image.png',
		metaRobots: 'this,that',
		mainTitle: 'mike article',
		url: '/test/article',
		mainParagraph: '<p>main</p>',
		body: '<p>body</p>',
		userId: 2,
		linkBlocks: [
			{
				title: 'link block 1',
				linkTitle: 'link block 1 link title',
				linkHref: '/href/1'
			},
			{
				title: 'link block 2',
				linkTitle: 'link block 2 link title',
				linkHref: '/href/2'
			}
		],
		navigations: [
			{
				title: 'top nav',
				paragraph: 'top para',
				isTop: true,
				links: [ { title: 'top link title', href: '/top/href' } ]
			},
			{
				title: 'bottom nav',
				paragraph: 'bottom para',
				isTop: false,
				links: [ { title: 'bottom link title', href: '/bottom/href' } ]
			}
		]
	}
]

function createArticle(articleData) {
	const article = Object.assign({}, articleData);
	const linkBlocks = article.linkBlocks;
	let navigations = article.navigations;
	let navLinks = [];
	navigations.map(nav => {
		navLinks.push(nav.links);
	});

	navigations.forEach(nav => {
		delete nav.links;
		return nav;
	});
	delete article.linkBlocks;
	delete article.navigations;

	return Article
		.insert(article)
		.then(([article]) => {
			linkBlocks.forEach(block => {
				block.articleId = article.id;
			});
			navigations.forEach(navigation => {
				navigation.articleId = article.id;
			});
			return Promise.all([
				LinkBlock.insert(linkBlocks),
				Navigation.insert(navigations)
			]);
		}).then(([linkBlocks, navigations]) => {
			navLinks[0].forEach(link => {
				link.navigationId = navigations[0].id;
			});
			navLinks[1].forEach(link => {
				link.navigationId = navigations[1].id;
			});
			const links = navLinks[0].concat(navLinks[1]);
			return Navigation.insertLink(links)
		});
}

function createArea(areaData) {
	let area = Object.assign({}, areaData);
	let counties = areaData.counties;
	let towns = areaData.counties.reduce((towns, county) => {
		return towns.concat(county.towns);
	}, []);

	delete area.counties;
	counties = counties.map(county => {
		let countyData = county;
		delete county.towns;
		return countyData;
	});

	return Area
		.insertArea(area)
		.then(([area]) => {
			counties.forEach(county => {
				county.areaId = area.id;
			});
			return Area.insertCounty(counties);
		}).then(counties => {
			const [county] = counties;
			towns.forEach(town => {
				town.countyId = county.id;
			});
			return Area.insertTown(towns);
		});

	console.log('area', area);
	console.log('counties', counties);
	console.log('towns', towns);
}

Promise.all([
	User.remove({}),
	Cottage.remove({})
]).then(() => {
	return bcrypt.hash('apples', 10);
}).then(hash => {
	const hashedUsers = users.map(user => {
		user.password = hash;
		return user;
	});
	return User.insert(users);
}).then(users => {
	let promises = [ users ];
	const createAreas = areas.map(area => createArea(area));
	promises = users.concat(createAreas);
	return Promise.all(promises);
}).then(([alex, mike, ...areas]) => {
	cottages[0].userId = alex.id;
	// cottages[1].userId = mike.id;
	// cottages[2].userId = alex.id;

	return Cottage.insert(cottages);
}).then(cottages => {
	return Promise.all(articles.map(article => createArticle(article)));                
}).then(articles => {
	knex.destroy();
}).catch(error => {
	console.log('samples failed', error);
});

// bcrypt
// 	.hash('apples', 10)
// 	.then(hash => {
// 		return User.remove({}):
// 	}).then(
// User
// 	.remove({})
// 	.then(() => {
// 		return User.insert(users);
// 	}).then(users => {
// 		return Cottage
// 			.remove({})
// 			.then(() => {
// 				return users;
// 			});
// 	}).then(users => {
// 		console.log(users);
// 		const [alex, mike] = users;
// 		cottages[0].userId = alex.id;
// 		cottages[1].userId = mike.id;
// 		cottages[2].userId = alex.id;
// 		return Cottage.insert(cottages);
// 	}).then(cottages => {
// 		console.log(cottages);
// 	});
