#!/bin/bash

tmux new-session -d -s dev

tmux rename-window 'editor'

tmux split-window -v -t 0
tmux resize-pane -y 5

tmux select-pane -t 0
tmux send-keys 'vim' 'C-m'

tmux -2 attach-session -t dev
