#/bin/bash

read -p 'User: ' user
read -p 'Database: ' db
read -sp 'Password: ' passwd

sudo -u postgres createuser $user

sudo -u postgres createdb -O $user $db
sudo -u postgres createdb -O $user "$db"_development
sudo -u postgres createdb -O $user "$db"_test

psql -U $user -d $db -c 'ALTER USER '"$user"' WITH PASSWORD '"'""$passwd""'";
