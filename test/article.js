const { knex, User, Article } = require('../models');
const assert = require('assert');
const request = require('supertest');
const app = require('../index');

describe('articles', function() {
	before(function() {
		return knex.migrate.latest().then(() => {
			const formUser = {
				username: 'mike',
				email: 'mike@mike.com',
				password: 'apples'
			}

			return request(app)
				.post('/admin/signup')
				.type('form')
				.send(formUser);
		});
	});

	it('should create article with required only', function() {
		return User
			.findOne({ username: 'mike' })
			.then(user => {
				return Article.insert({
					mainTitle: 'have nice weekend',
					url: 'test-article.htm',
					userId: user.id
				});
			}).then(articles => {
				assert.equal(articles.length, 1);

				const [article] = articles;
				assert.equal(article.metaRobots, 'index,robots');
				assert.equal(article.mainTitle, 'have nice weekend');
				assert.equal(article.url, 'test-article.htm');
			});
	});

	it('should create article with everything', function() {
		return User
			.findOne({ username: 'mike' })
			.then(user => {
				return Article
					.insert({
						metaTitle: 'meta title',
						metaDescription: 'meta description',
						metaKeywords: 'keywords',
						metaImage: 'image',
						metaRobots: 'this,that',
						mainTitle: 'full article',
						url: 'full-article.htm',
						mainParagraph: 'para',
						body: '<p>body</p>',
						userId: user.id
					});
			}).then(articles => {
				assert.equal(articles.length, 1);

				const [article] = articles;

				assert.equal(article.metaTitle, 'meta title');
				assert.equal(article.metaDescription, 'meta description');
				assert.equal(article.metaKeywords, 'keywords');
				assert.equal(article.metaImage, 'image');
				assert.equal(article.metaRobots, 'this,that');
				assert.equal(article.mainTitle, 'full article');
				assert.equal(article.url, 'full-article.htm');
			});
	});

	it('should create many articles', function() {
		return User
			.findOne({ username: 'mike' })
			.then(user => {
				return Article.insert([
					{
						mainTitle: 'read this',
						url: 'read-this.htm',
						userId: user.id
					},
					{
						mainTitle: 'read that',
						url: 'read-that.htm',
						userId: user.id
					}
				]);
			}).then(articles => {
				const [article1, article2] = articles;

				assert.equal(articles.length, 2);
				assert.equal(article1.mainTitle, 'read this');
				assert.equal(article1.url, 'read-this.htm');
				assert.equal(article2.mainTitle, 'read that');
				assert.equal(article2.url, 'read-that.htm');
			});
	});

	it('should find article', function() {
		return Article
			.find({
				mainTitle: 'read this'
			}).then(articles => {
				const [article] = articles;
				assert.equal(articles.length, 1);
				assert.equal(article.mainTitle, 'read this');
			});
	});

	it('should find one article', function() {
		return Article
			.findOne({ mainTitle: 'read that' })
			.then(article => {
				assert.equal(article.mainTitle, 'read that');
			});
	});

	it('should find all articles', function() {
		return Article
			.findAll()
			.then(articles => {
				assert.equal(articles.length, 4);
			});
	});

	it('should update article', function() {
		return Article
			.update({
				mainTitle: 'read this changed'
			}, {
				mainTitle: 'read this'
			}).then(articles => {
				const [article] = articles;
				assert.equal(articles.length, 1);
				assert.equal(article.mainTitle, 'read this changed');
			});
	});

	it('should remove article', function() {
		return Article
			.remove({
				mainTitle: 'read that'
			}).then(deleted => {
				assert.equal(deleted.length, 1);
				return Article.findOne({ mainTitle: 'read this' });
			}).then(article => {
				assert.equal(article, undefined);
				return Article.findAll();
			}).then(articles => {
				assert.equal(articles.length, 3);
			});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
