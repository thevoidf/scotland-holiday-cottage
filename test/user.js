const { knex, User } = require('../models');
const assert = require('assert');

describe('users', function() {
	before(function() {
		return knex.migrate.latest();
	});

	it('should create user', function(done) {
		User
			.insert({
				username: 'alex',
				email: 'alex@mail.com',
				password: 'apples'
			})
			.then(users => {
				const [user] = users;

				assert.equal(users.length, 1);
				assert.equal(user.username, 'alex');
				assert.equal(user.email, 'alex@mail.com');
				assert.equal(user.isAdmin, false);
				assert.equal(user.password, 'apples');
				done();
			}).catch(error => done(error));
	});

	it('should create many users', function(done) {
		User
			.insert([
				{
					username: 'mike',
					email: 'mike@mail.com',
					password: 'apples'
				},
				{
					username: 'tj',
					email: 'tj@mail.com',
					isAdmin: true,
					password: 'apples'
				}
			])
			.then(users => {
				const [mike, tj] = users;

				assert.equal(users.length, 2);

				assert.equal(mike.username, 'mike');
				assert.equal(mike.email, 'mike@mail.com');
				assert.equal(mike.isAdmin, false);
				assert.equal(mike.password, 'apples');

				assert.equal(tj.username, 'tj');
				assert.equal(tj.email, 'tj@mail.com');
				assert.equal(tj.isAdmin, true);
				assert.equal(tj.password, 'apples');
				done();
			}).catch(error => done(error));
	});

	it('should find user', function(done) {
		User
			.find({ username: 'tj' })
			.then(users => {
				const [tj] = users;

				assert.equal(tj.username, 'tj');
				assert.equal(tj.email, 'tj@mail.com');
				assert.equal(tj.isAdmin, true);
				assert.equal(tj.password, 'apples');
				done();
			}).catch(error => done(error));
	});

	it('should find one user', function(done) {
		User
			.findOne({ username: 'tj' })
			.then(tj => {
				assert.equal(tj.username, 'tj');
				assert.equal(tj.email, 'tj@mail.com');
				assert.equal(tj.isAdmin, true);
				assert.equal(tj.password, 'apples');
				done();
			}).catch(error => done(error));
	});

	it('should find all users', function(done) {
		User
			.findAll()
			.then(users => {
				assert.equal(users.length, 3);
				done();
			}).catch(error => done(error));
	});

	it('should update user', function(done) {
		User
			.update({ username: 'tj' }, { email: 'tj@changed.com' })
			.then(users => {
				User
					.findOne({ username: 'tj' }, ['email'])
					.then(tj => {
						assert.equal(tj.email, 'tj@changed.com');
						done();
					}).catch(error => done(error));
			}).catch(error => done(error));
	});

	it('should remove user', function(done) {
		User
			.remove({ email: 'mike@mail.com' })
			.then(deleted => {
				assert.equal(deleted.length, 1);
				return deleted;
			}).then(deleted => {
				return User
					.findOne({ email: 'mike@mail.com' })
					.then(user => {
						assert.equal(user, undefined);
					});
			}).then(() => {
				User
					.findAll()
					.then(users => {
						assert.equal(users.length, 2);
						done();
					});
			}).catch(error => done(error));
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
