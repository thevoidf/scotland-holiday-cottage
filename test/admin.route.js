const { knex, User, Area, Cottage, Article, LinkBlock, Navigation } = require('../models');
const request = require('supertest');
const assert = require('assert');
const app = require('../index');

describe('admin', function() {
	before(function() {
		return knex.migrate.latest();
	});

	it('should signup user', function() {
		const formUser = {
			username: 'mike',
			email: 'mike@mike.com',
			password: 'apples'
		}

		return request(app)
			.post('/admin/signup')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return User.findAll();
			}).then(users => {
				assert.equal(users.length, 1);
			});
	});

	it('should login user', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}

		return request(app)
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
			});
	});

	it('should post area from form', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}
		const formArea = {
			name: 'morodor',
			slug: 'morodor-slug',
			'counties[0][name]': 'morodor county',
			'counties[0][slug]': 'morodor-county-slug',
			'counties[0][towns][0][name]': 'morodor town',
			'counties[0][towns][0][slug]': 'morodor-town-slug'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return session
					.post('/admin/areas/post')
					.type('form')
					.send(formArea)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/areas');
				return Area.findAllArea();
			}).then(areas => {
				assert.equal(areas.length, 1);
				const [area] = areas;

				assert.equal(area.name, 'morodor');
				assert.equal(area.slug, 'morodor-slug');

				return Area.findCounty({ areaId: area.id });
			}).then(counties => {
				assert.equal(counties.length, 1);
				const [county] = counties;

				assert.equal(county.name, 'morodor county');
				assert.equal(county.slug, 'morodor-county-slug');

				return Area.findTown({ countyId: county.id });
			}).then(towns => {
				assert.equal(towns.length, 1);
				const [town] = towns;

				assert.equal(town.name, 'morodor town');
				assert.equal(town.slug, 'morodor-town-slug');
			});
	});

	it('should post cottage from form', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}
		const formCottage = {
			area: 'morodor',
			county: 'morodor county',
			town: 'morodor town',
			county: 'morodor county',
			slug: 'morodor-slug',
			'listing-title': 'title list'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return session
					.post('/admin/cottages/post')
					.type('form')
					.send(formCottage)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/cottages');
				return Cottage.findAll();
			}).then(cottages => {
				assert.equal(cottages.length, 1);
				const [cottage] = cottages;

				return Promise.all([
					cottage,
					Area.findOneArea({ name: formCottage.area }),
					Area.findOneCounty({ name: formCottage.county }),
					Area.findOneTown({ name: formCottage.town })
				]);
			}).then(([cottage, area, county, town]) => {
				assert.equal(cottage.areaId, area.id);
				assert.equal(cottage.countyId, county.id);
				assert.equal(cottage.townId, town.id);
				assert.equal(cottage.slug, formCottage.slug);
				assert.equal(cottage.listingTitle, formCottage['listing-title']);
				assert.equal(cottage.isPublished, false);
			});
	});

	it('should update cottage from form', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}

		const formCottage = {
			area: 'morodor',
			county: 'morodor county',
			town: 'morodor town',
			county: 'morodor county',
			slug: 'morodor-slug-updated',
			'listing-title': 'title list updated',
			'is-published': 'on'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return Cottage.findOne({
					slug: 'morodor-slug'
				});
			}).then(cottage => {
				return session
					.post(`/admin/cottages/${cottage.id}/update`)
					.type('form')
					.send(formCottage)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/cottages');
				return Cottage.findOne({
					slug: 'morodor-slug-updated'
				});
			}).then(cottage => {
				return Promise.all([
					cottage,
					Area.findOneArea({ name: formCottage.area }),
					Area.findOneCounty({ name: formCottage.county }),
					Area.findOneTown({ name: formCottage.town })
				]);
			}).then(([cottage, area, county, town]) => {
				assert.equal(cottage.areaId, area.id);
				assert.equal(cottage.countyId, county.id);
				assert.equal(cottage.townId, town.id);
				assert.equal(cottage.slug, formCottage.slug);
				assert.equal(cottage.listingTitle, formCottage['listing-title']);
				assert.equal(cottage.isPublished, true);
			});
	});

	it('should delete cottage', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return Cottage.findOne({
					slug: 'morodor-slug-updated'
				});
			}).then(cottage => {
				return session
					.post(`/admin/cottages/${cottage.id}/delete`)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/cottages');
				return Cottage.findAll();
			}).then(cottages => {
				assert.equal(cottages.length, 0);
			});
	});

	it('should post article from form', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}
		const formArticle = {
			'main-title': 'test article',
			url: '/test/article',

			'link-blocks[0][title]': 'test link block',
			'link-blocks[0][link-title]': 'test links link title',
			'link-blocks[0][link-href]': '/test/link',
			'link-blocks[1][title]': 'test link block 2',
			'link-blocks[1][link-title]': 'test links link title 2',
			'link-blocks[1][link-href]': '/test/link2',

			'navigations[top][title]': 'top nav',
			'navigations[top][paragraph]': 'para',
			'navigations[top][links][0][title]': 'top nav link title',
			'navigations[top][links][0][href]': '/top/nav/link',

			'navigations[bottom][title]': 'bottom nav',
			'navigations[bottom][paragraph]': 'para',
			'navigations[bottom][links][1][title]': 'bottom nav link title',
			'navigations[bottom][links][1][href]': '/bottom/nav/link'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return session
					.post('/admin/articles/post')
					.type('form')
					.send(formArticle)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return Article.findAll();
			}).then(articles => {
				assert.equal(articles.length, 1);
				const [article] = articles;

				assert.equal(article.mainTitle, 'test article');
				assert.equal(article.url, '/test/article');
				return LinkBlock.findAll();
			}).then(linkBlocks => {
				assert.equal(linkBlocks.length, 2);
				const [linkBlock1, linkBlock2] = linkBlocks;

				assert.equal(linkBlock1.title, 'test link block');
				assert.equal(linkBlock1.linkTitle, 'test links link title');
				assert.equal(linkBlock1.linkHref, '/test/link');

				assert.equal(linkBlock2.title, 'test link block 2');
				assert.equal(linkBlock2.linkTitle, 'test links link title 2');
				assert.equal(linkBlock2.linkHref, '/test/link2');

				return Navigation.findAll();
			}).then(navigations => {
				assert.equal(navigations.length, 2);
				const [nav1, nav2] = navigations;

				assert.equal(nav1.title, 'top nav');
				assert.equal(nav1.paragraph, 'para');
				assert.equal(nav1.isTop, true);

				assert.equal(nav2.title, 'bottom nav');
				assert.equal(nav2.paragraph, 'para');
				assert.equal(nav2.isTop, false);

				return Promise.all([
					Navigation.findLink({ navigationId: nav1.id }),
					Navigation.findLink({ navigationId: nav2.id })
				]);
			}).then(([nav1Links, nav2Links]) => {
				const [nav1Link] = nav1Links;
				const [nav2Link] = nav2Links;

				assert.equal(nav1Links.length, 1);
				assert.equal(nav2Links.length, 1);
				assert.equal(nav1Link.title, 'top nav link title');
				assert.equal(nav1Link.href, '/top/nav/link');
				assert.equal(nav2Link.title, 'bottom nav link title');
				assert.equal(nav2Link.href, '/bottom/nav/link');
			});
	});

	it('should update article from form', function() {
		const formUser = {
			email: 'mike@mike.com',
			password: 'apples'
		}
		const formArticle = {
			'main-title': 'test article updated',
			url: '/test/article/updated',

			'link-blocks[0][title]': 'test link block updated',
			'link-blocks[0][link-title]': 'test links link title updated',
			'link-blocks[0][link-href]': '/test/link/updated',
			'link-blocks[1][title]': 'test link block 2 updated',
			'link-blocks[1][link-title]': 'test links link title 2 updated',
			'link-blocks[1][link-href]': '/test/link2/updated',

			'navigations[top][title]': 'top nav updated',
			'navigations[top][paragraph]': 'para updated',
			'navigations[top][links][0][title]': 'top nav link title updated',
			'navigations[top][links][0][href]': '/top/nav/link/updated',

			'navigations[bottom][title]': 'bottom nav updated',
			'navigations[bottom][paragraph]': 'para updated',
			'navigations[bottom][links][1][title]': 'bottom nav link title updated',
			'navigations[bottom][links][1][href]': '/bottom/nav/link/updated'
		}

		const session = request.agent(app);

		return session
			.post('/admin')
			.type('form')
			.send(formUser)
			.expect(302)
			.then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return session
					.post(`/admin/articles/${1}/update`)
					.type('form')
					.send(formArticle)
					.expect(302);
			}).then(result => {
				assert.equal(result.header['location'], '/admin/dashboard');
				return Article.findAll();
			}).then(articles => {
				assert.equal(articles.length, 1);
				const [article] = articles;

				assert.equal(article.mainTitle, 'test article updated');
				assert.equal(article.url, '/test/article/updated');
				return LinkBlock.findAll();
			}).then(linkBlocks => {
				assert.equal(linkBlocks.length, 2);
				const [linkBlock1, linkBlock2] = linkBlocks;

				assert.equal(linkBlock1.title, 'test link block updated');
				assert.equal(linkBlock1.linkTitle, 'test links link title updated');
				assert.equal(linkBlock1.linkHref, '/test/link/updated');

				assert.equal(linkBlock2.title, 'test link block 2 updated');
				assert.equal(linkBlock2.linkTitle, 'test links link title 2 updated');
				assert.equal(linkBlock2.linkHref, '/test/link2/updated');

				return Navigation.findAll();
			}).then(navigations => {
				assert.equal(navigations.length, 2);
				const [nav1, nav2] = navigations;

				assert.equal(nav1.title, 'top nav updated');
				assert.equal(nav1.paragraph, 'para updated');
				assert.equal(nav1.isTop, true);

				assert.equal(nav2.title, 'bottom nav updated');
				assert.equal(nav2.paragraph, 'para updated');
				assert.equal(nav2.isTop, false);

				return Promise.all([
					Navigation.findLink({ navigationId: nav1.id }),
					Navigation.findLink({ navigationId: nav2.id })
				]);
			}).then(([nav1Links, nav2Links]) => {
				const [nav1Link] = nav1Links;
				const [nav2Link] = nav2Links;

				assert.equal(nav1Links.length, 1);
				assert.equal(nav2Links.length, 1);
				assert.equal(nav1Link.title, 'top nav link title updated');
				assert.equal(nav1Link.href, '/top/nav/link/updated');
				assert.equal(nav2Link.title, 'bottom nav link title updated');
				assert.equal(nav2Link.href, '/bottom/nav/link/updated');
			});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
