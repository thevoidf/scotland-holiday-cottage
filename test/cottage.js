const { knex, Area, Cottage } = require('../models');
const assert = require('assert');

describe('cottages', function() {
	before(function() {
		return knex.migrate.latest().then(() => {
			return knex('users').insert({
				username: 'alex',
				email: 'alex@mail.com',
				password: 'apples'
			});
		}).then(user => {
			return Area.insertArea({
				name: 'morodor',
				slug: 'morodor-slug'
			});
		}).then(([area]) => {
			return Area.insertCounty({
				name: 'morodor county',
				slug: 'morodor-county-slug',
				areaId: area.id
			});
		}).then(([county]) => {
			return Area.insertTown({
				name: 'morodor town',
				slug: 'morodor-town-slug',
				countyId: county.id
			});
		});
	});

	function getArea() {
		return Area
			.findOneArea({ name: 'morodor' })
			.then(area => {
				return Promise.all([
					area,
					Area.findOneCounty({ areaId: area.id })
				]);
			}).then(([area, county]) => {
				return Promise.all([
					area,
					county,
					Area.findOneTown({ countyId: county.id })
				]);
			});
	}

	it('should create cottage', function() {
		getArea().then(([area, county, town]) => {
			return Promise.all([
				area, county, town,
				Cottage.insert({
					userId: 1,
					areaId: area.id,
					countyId: county.id,
					townId: town.id,
					slug: 'morodor-cottage',
					listingTitle: 'morodor list'
				})
			]).then(([area, county, town, cottages]) => {
				const [cottage] = cottages;

				assert.equal(cottages.length, 1);

				assert.equal(cottage.areaId, area.id);
				assert.equal(cottage.countyId, county.id);
				assert.equal(cottage.townId, town.id);
				assert.equal(cottage.slug, 'morodor-cottage');
				assert.equal(cottage.listingTitle, 'morodor list');
				assert.equal(cottage.isPublished, false);
			});
		});
	});

	it('should create many cottages', function() {
		return getArea().then(([area, county, town]) => {
			return Promise.all([
				area, county, town,
				Cottage.insert([
					{
						userId: 1,
						areaId: area.id,
						countyId: county.id,
						townId: town.id,
						slug: 'argyll-cottage',
						listingTitle: 'argyll list'
					},
					{
						userId: 1,
						areaId: area.id,
						countyId: county.id,
						townId: town.id,
						slug: 'highland-cottage',
						listingTitle: 'highland list'
					}
				])
			]);
		}).then(([area,county, town, cottages]) => {
			const [argyll, highland] = cottages;

			assert.equal(cottages.length, 2);

			assert.equal(argyll.userId, 1);
			assert.equal(argyll.areaId, area.id);
			assert.equal(argyll.countyId, county.id);
			assert.equal(argyll.townId, town.id);
			assert.equal(argyll.slug, 'argyll-cottage');
			assert.equal(argyll.listingTitle, 'argyll list');
			assert.equal(argyll.isPublished, false);

			assert.equal(highland.userId, 1);
			assert.equal(highland.areaId, area.id);
			assert.equal(highland.countyId, county.id);
			assert.equal(highland.townId, town.id);
			assert.equal(highland.slug, 'highland-cottage');
			assert.equal(highland.listingTitle, 'highland list');
			assert.equal(highland.isPublished, false);
		});
	});

	it('should find cottage', function() {
		return Cottage
			.find({
				slug: 'highland-cottage',
			}).then(cottages => {
				const [cottage] = cottages;

				assert.equal(cottages.length, 1);

				assert.equal(cottage.userId, 1);
				assert.equal(cottage.slug, 'highland-cottage');
				assert.equal(cottage.listingTitle, 'highland list');
				assert.equal(cottage.isPublished, false);
			});
	});

	it('should find one cottage', function() {
		return Cottage
			.findOne({ slug: 'argyll-cottage' })
			.then(cottage => {
				assert.equal(cottage.slug, 'argyll-cottage');
			});
	});

	it('should find all cottages', function() {
		return Cottage
			.findAll()
			.then(cottages => {
				assert.equal(cottages.length, 3);
			});
	});

	it('should update cottage', function() {
		return Cottage
			.update({
				listingTitle: 'highland list edited'
			}, {
				listingTitle: 'highland list'
			}).then(cottages => {
				const [cottage] = cottages;

				assert.equal(cottages.length, 1);
				assert.equal(cottage.listingTitle, 'highland list edited');
			});
	});

	it('should remove cottage', function() {
		return Cottage
			.remove({
				slug: 'argyll-cottage'
			}).then(deleted => {
				assert.equal(deleted.length, 1);
				return deleted;
			}).then(deleted => {
				return Cottage
					.findOne({ slug: 'argyll-cottage' })
					.then(cottage => {
						assert.equal(cottage, undefined);
					});
			}).then(() => {
				return Cottage
					.find({})
					.then(cottages => {
						assert.equal(cottages.length, 2);
					});
			});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
