const { knex, User, Article, LinkBlock } = require('../models');
const assert = require('assert');
const request = require('supertest');
const app = require('../index');

describe('link blocks', function() {
	before(function() {
		return knex.migrate.latest().then(() => {
			const formUser = {
				username: 'mike',
				email: 'mike@mike.com',
				password: 'apples'
			}

			return request(app)
				.post('/admin/signup')
				.type('form')
				.send(formUser)
				.expect(302)
				.then(result => {
					assert.equal(result.header['location'], '/admin/dashboard');
					return User.findAll();
				}).then(users => {
					const [user] = users;
					assert.equal(users.length, 1);

					return Article.insert({
						mainTitle: 'have nice weekend',
						url: 'test-article.htm',
						userId: user.id
					});
				});
		});
	});

	it('should create link block', function() {
		return Article
			.findOne({ url: 'test-article.htm' })
			.then(article => {
				return LinkBlock.insert({
					title: 'title',
					linkTitle: 'link title',
					linkHref: '/link/href',
					articleId: article.id
				})
			}).then(linkBlocks => {
				assert.equal(linkBlocks.length, 1);

				const [linkBlock] = linkBlocks;
				assert.equal(linkBlock.title, 'title');
				assert.equal(linkBlock.linkTitle, 'link title');
				assert.equal(linkBlock.linkHref, '/link/href');
			});
	});

	it('should create many link blocks', function() {
		return Article
			.findOne({ url: 'test-article.htm' })
			.then(article => {
				return LinkBlock.insert([
					{
						title: 'link block',
						linkTitle: 'link block title',
						linkHref: '/link/href/block1',
						articleId: article.id
					},
					{
						title: 'link block 2',
						linkTitle: 'link block title 2',
						linkHref: '/link/href/block2',
						articleId: article.id
					}
				]);
			}).then(linkBlocks => {
				const [linkBlock1, linkBlock2] = linkBlocks;

				assert.equal(linkBlocks.length, 2);

				assert.equal(linkBlock1.title, 'link block');
				assert.equal(linkBlock1.linkTitle, 'link block title');
				assert.equal(linkBlock1.linkHref, '/link/href/block1');

				assert.equal(linkBlock2.title, 'link block 2');
				assert.equal(linkBlock2.linkTitle, 'link block title 2');
				assert.equal(linkBlock2.linkHref, '/link/href/block2');
			});
	});

	it('should find link block', function() {
		return LinkBlock
			.find({
				title: 'link block'
			}).then(linkBlocks => {
				const [linkBlock] = linkBlocks;
				assert.equal(linkBlocks.length, 1);
				assert.equal(linkBlock.title, 'link block');
			});
	});

	it('should find one link block', function() {
		return LinkBlock
			.findOne({ title: 'link block' })
			.then(linkBlock => {
				assert.equal(linkBlock.title, 'link block');
			});
	});

	it('should find all link blocks', function() {
		return LinkBlock
			.findAll()
			.then(linkBlocks => {
				assert.equal(linkBlocks.length, 3);
			});
	});

	it('should update link block', function() {
		return LinkBlock
			.update({
				title: 'link block changed'
			}, {
				title: 'link block'
			}).then(linkBlocks => {
				const [linkBlock] = linkBlocks;
				assert.equal(linkBlocks.length, 1);
				assert.equal(linkBlock.title, 'link block changed');
			});
	});

	it('should remove link block', function() {
		return LinkBlock
			.remove({
				title: 'link block 2'
			}).then(deleted => {
				assert.equal(deleted.length, 1);
				return LinkBlock.findOne({ title: 'link block 2' });
			}).then(linkBlock => {
				assert.equal(linkBlock, undefined);
				return LinkBlock.findAll();
			}).then(linkBlocks => {
				assert.equal(linkBlocks.length, 2);
			});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
