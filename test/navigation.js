const { knex, User, Article, Navigation } = require('../models');
const assert = require('assert');
const request = require('supertest');
const app = require('../index');

describe('navigations', function() {
	before(function() {
		return knex.migrate.latest().then(() => {
			const formUser = {
				username: 'mike',
				email: 'mike@mike.com',
				password: 'apples'
			}

			return request(app)
				.post('/admin/signup')
				.type('form')
				.send(formUser)
				.expect(302)
				.then(result => {
					return User.findAll();
				}).then(([user]) => {
					return Article.insert({
						mainTitle: 'test article',
						url: 'test-article.htm',
						userId: user.id
					});
				});
		});
	});

	it('should create navigation', function() {
		Article
			.findOne({ mainTitle: 'test article' })
			.then(article => {
				return Navigation.insert({
					title: 'test nav',
					paragraph: 'para',
					isTop: true,
					articleId: article.id
				})
			}).then(navigations => {
				assert.equal(navigations.length, 1);

				const [navigation] = navigations;
				assert.equal(navigation.title, 'test nav');
				assert.equal(navigation.paragraph, 'para');
				assert.equal(navigation.isTop, true);
			});
	});

	it('should create many navigations', function() {
		return Article
			.findOne({ mainTitle: 'test article' })
			.then(article => {
				return Navigation.insert([
					{
						title: 'test nav 2',
						paragraph: 'para 2',
						isTop: true,
						articleId: article.id
					},
					{
						title: 'test nav 3',
						paragraph: 'para 3',
						isTop: false,
						articleId: article.id
					}
				]).then(navigations => {
					const [navigation1, navigation2] = navigations;

					assert.equal(navigations.length, 2);

					assert.equal(navigation1.title, 'test nav 2');
					assert.equal(navigation1.paragraph, 'para 2');
					assert.equal(navigation1.isTop, true);

					assert.equal(navigation2.title, 'test nav 3');
					assert.equal(navigation2.paragraph, 'para 3');
					assert.equal(navigation2.isTop, false);
				});
			});
	});

	it('should find navigation', function() {
		return Navigation
			.find({
				title: 'test nav 2'
			}).then(navigations => {
				const [navigation] = navigations;
				assert.equal(navigations.length, 1);
				assert.equal(navigation.title, 'test nav 2');
			});
	});

	it('should find one navigation', function() {
		return Navigation
			.findOne({ title: 'test nav 3' })
			.then(navigation => {
				assert.equal(navigation.title, 'test nav 3');
			});
	});

	it('should find all navigations', function() {
		return Navigation
			.findAll()
			.then(navigations => {
				assert.equal(navigations.length, 3);
			});
	});

	it('should update navigation', function() {
		return Navigation
			.update({
				title: 'test nav 2 changed'
			}, {
				title: 'test nav 2'
			}).then(navigations => {
				const [navigation] = navigations;
				assert.equal(navigations.length, 1);
				assert.equal(navigation.title, 'test nav 2 changed');
			});
	});

	it('should create navigation link', function() {
		return Navigation
			.findOne({ title: 'test nav' })
			.then(navigation => {
				assert.notEqual(navigation, undefined);
				return Navigation
					.insertLink({
						title: 'nav link',
						href: '/nav/link',
						navigationId: navigation.id
					});
			}).then(navigationLinks => {
				assert.equal(navigationLinks.length, 1);
				const [navigationLink] = navigationLinks;
				assert.equal(navigationLink.title, 'nav link');
				assert.equal(navigationLink.href, '/nav/link');
				assert.equal(navigationLink.navigationId, 1);
			});
	});


	it('should find all navigation links', function() {
		return Navigation
			.findOne({ title: 'test nav' })
			.then(navigation => {
				return Navigation.findLink({
					navigationId: navigation.id
				});
			}).then(navigationLinks => {
				assert.equal(navigationLinks.length, 1);
				const [navigationLink] = navigationLinks;
				assert.equal(navigationLink.title, 'nav link');
				assert.equal(navigationLink.href, '/nav/link');
				assert.equal(navigationLink.navigationId, 1);
			});
	});

	it('should remove navigation', function() {
		return Navigation
			.remove({
				title: 'test nav 3'
			}).then(deleted => {
				assert.equal(deleted.length, 1);
				return Navigation.findOne({ title: 'test nav 3' });
			}).then(navigation => {
				assert.equal(navigation, undefined);
				return Navigation.findAll();
			}).then(navigations => {
				assert.equal(navigations.length, 2);
			});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
