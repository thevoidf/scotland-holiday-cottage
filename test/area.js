const { knex, User, Area, Cottage } = require('../models');
const assert = require('assert');

describe('areas', function() {
	before(function() {
		return knex.migrate.latest();
	});

	it('should create area, county and town', function() {
		return Area.insertArea({
			name: 'area 51',
			slug: 'area-51'
		}).then(([area]) => {
			assert.equal(area.name, 'area 51');
			assert.equal(area.slug, 'area-51');
			return Area.insertCounty({
				name: 'county 101',
				slug: 'county-101',
				areaId: area.id
			});
		}).then(([county]) => {
			assert.equal(county.name, 'county 101');
			assert.equal(county.slug, 'county-101');
			return Area.insertTown({
				name: 'town 12',
				slug: 'town-12',
				countyId: county.id
			});
		}).then(([town]) => {
			assert.equal(town.name, 'town 12');
			assert.equal(town.slug, 'town-12');
		});
	});

	after(function() {
		return knex.migrate.rollback();
	});
});
