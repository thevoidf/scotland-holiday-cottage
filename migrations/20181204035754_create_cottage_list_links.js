exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_list_links', t => {
		t.increments('id').primary();
		t.string('title');
		t.string('href').notNullable();
		t.boolean('is_left').default(false);
		t.timestamp('created_at').default(knex.fn.now());
		t.integer('cottage_list_id')
			.references('cottage_lists.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_list_links');
};
