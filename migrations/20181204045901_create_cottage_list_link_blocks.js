exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_list_link_blocks', t => {
		t.increments('id').primary();
		t.string('title');
		t.string('link_title');
		t.string('link_href').notNullable();
		t.integer('cottage_list_id')
			.references('cottage_lists.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_list_link_blocks');
};
