exports.up = function(knex, Promise) {
	return knex.schema.createTable('navigation_links', t => {
		t.increments('id').primary();
		t.string('title');
		t.string('href').notNullable();
		t.integer('navigation_id')
			.references('navigations.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('navigation_links');
};
