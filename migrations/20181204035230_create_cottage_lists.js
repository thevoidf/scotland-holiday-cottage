exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_lists', t => {
		t.increments('id').primary();
		t.string('meta_title');
		t.string('meta_description');
		t.string('meta_image');
		t.string('main_title').notNullable();
		t.string('slug').notNullable();
		t.text('main_paragraph');
		t.text('bottom_description');
		t.text('schema_description');
		t.boolean('is_published').default(false);
		t.timestamp('created_at').default(knex.fn.now());
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_lists');
};
