exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_list_cottages', t => {
		t.increments('id').primary();
		t.integer('cottage_id')
			.references('cottages.id')
			.onDelete('CASCADE')
			.notNullable();
		t.integer('cottage_list_id')
			.references('cottage_lists.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_list_cottages');
};
