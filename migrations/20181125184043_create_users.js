exports.up = function(knex, Promise) {
	return knex.schema.createTable('users', function(t) {
		t.increments('id').primary();
		t.string('username').unique().notNullable();
		t.string('email').unique().notNullable();
		t.boolean('is_admin').notNullable().default(false);
		t.string('password').notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('users');
}
