exports.up = function(knex, Promise) {
	return knex.schema.createTable('filter_properties', function(t) {
		t.increments('id').primary();
		t.string('title').notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('filter_properties');
}
