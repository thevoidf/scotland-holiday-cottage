exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_filter_properties', function(t) {
		t.increments('id').primary();
		t.integer('cottage_id')
			.references('cottages.id')
			.onDelete('CASCADE')
			.notNullable();
		t.integer('filter_id')
			.references('filter_properties.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_filter_properties');
}
