exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_areas', t => {
		t.increments('id').primary();
		t.string('name').notNullable();
		t.string('slug');
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_areas');
};
