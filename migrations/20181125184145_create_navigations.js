exports.up = function(knex, Promise) {
	return knex.schema.createTable('navigations', t => {
		t.increments('id').primary();
		t.string('title');
		t.text('paragraph');
		t.boolean('is_top');
		t.integer('article_id')
			.references('articles.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('navigations');
};
