exports.up = function(knex, Promise) {
	return knex.schema.createTable('special_offers', function(t) {
		t.increments('id').primary();
		t.string('offer').notNullable();
		t.date('start_date').notNullable();
		t.date('end_date').notNullable();
		t.string('old_price');
		t.string('new_price');
		t.timestamp('created_at').default(knex.fn.now());
		t.integer('cottage_id')
			.references('cottages.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('special_offers');
}
