exports.up = function(knex, Promise) {
	return knex.schema.createTable('link_blocks', t => {
		t.increments('id').primary();
		t.string('title');
		t.string('link_title');
		t.string('link_href').notNullable();
		t.integer('article_id').references('articles.id').onDelete('CASCADE').notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('link_blocks');
};
