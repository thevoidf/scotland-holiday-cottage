exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottages', t => {
		/*
		 * + title string not req x
		 * + sort value num x
		 * + sleeps num x
		 * + add_sleeps num x
		 * + bedrooms num x
		 * + pets number x
		 * + disabled_access bool to disabled to desacbled_accsess x
		 * + ad_random | is_advertisable boolean x
		 * + sort_order_in_ad x
		 * + ics_sources text x
		 * + table filter_proerties ?
		 *
		 * [{"group_id":1,"ics":[],"title":"","desc":""}] x
		*/
		t.increments('id').primary();
		t.string('title');
		t.string('slug').notNullable();
		t.integer('sort_value');
		t.integer('ad_sort_order');
		t.integer('sleeps');
		t.integer('add_sleeps');
		t.integer('bedrooms');
		t.integer('pets');
		t.text('ics_sources').default('[{"group_id":1,"ics":[],"title":"","desc":""}]');
		t.boolean('disabled_access').default(false);
		t.boolean('is_published').default(false);
		t.boolean('is_advertisable').default(false);

		t.string('listing_title').notNullable();
		t.string('listing_image_url');
		t.string('listing_image_alt');
		t.text('listing_desc');
		t.string('listing_link_name');

		t.string('primary_meta_title');
		t.text('primary_meta_desc');
		t.string('primary_main_image_url');
		t.string('primary_main_image_alt');
		t.string('primary_main_title');
		t.text('primary_schema_desc');
		t.text('primary_body');

		t.string('enquiry_meta_title');
		t.text('enquiry_meta_desc');
		t.string('enquiry_main_image_url');
		t.string('enquiry_main_image_alt');
		t.string('enquiry_main_title');
		t.text('enquiry_schema_desc');
		t.text('enquiry_body');

		t.string('location_meta_title');
		t.text('location_meta_desc');
		t.string('location_meta_image_url');
		t.string('location_meta_image_alt');
		t.string('location_main_title');
		t.text('location_schema_desc');
		t.text('location_body');
		t.decimal('location_lat');
		t.decimal('location_lng');

		t.timestamp('created_at').default(knex.fn.now());

		t.integer('area_id')
			.references('cottage_areas.id')
			.onDelete('CASCADE')
			.notNullable();
		t.integer('county_id')
			.references('cottage_counties.id')
			.onDelete('CASCADE')
			.notNullable();
		t.integer('town_id')
			.references('cottage_towns.id')
			.onDelete('CASCADE')
			.notNullable();

		t.integer('user_id')
			.references('users.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottages');
};
