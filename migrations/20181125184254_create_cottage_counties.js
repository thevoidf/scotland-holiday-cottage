exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_counties', t => {
		t.increments('id').primary();
		t.string('name').notNullable();
		t.string('slug');
		t.integer('area_id')
			.references('cottage_areas.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_counties');
};
