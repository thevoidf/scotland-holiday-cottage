exports.up = function(knex, Promise) {
	return knex.schema.createTable('articles', t => {
		t.increments('id').primary();
		t.string('meta_title');
		t.string('meta_description');
		t.string('meta_keywords');
		t.string('meta_image');
		t.string('meta_robots').default('index,robots');
		t.string('main_title').notNullable();
		t.string('url').notNullable();
		t.text('main_paragraph');
		t.text('body');
		t.text('schema_description');
		t.boolean('is_published').default(false);
		t.timestamp('created_at').default(knex.fn.now());
		t.integer('user_id').references('users.id').onDelete('CASCADE').notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('articles');
}
