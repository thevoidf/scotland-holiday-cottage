exports.up = function(knex, Promise) {
	return knex.schema.createTable('cottage_towns', t => {
		t.increments('id').primary();
		t.string('name').notNullable();
		t.string('slug');
		t.integer('county_id')
			.references('cottage_counties.id')
			.onDelete('CASCADE')
			.notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('cottage_towns');
};
