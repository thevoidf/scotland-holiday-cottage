const path = require('path');
require('dotenv').config({
	path: path.join(__dirname, '../.env')
});
const migrationsDir = path.join(__dirname, '../migrations');
const { DB_HOST, DB_DATABASE, DB_USER, DB_PASSWORD } = process.env;

module.exports = {
	development: {
		client: 'pg',
		connection: {
			host: DB_HOST,
			database: DB_DATABASE + '_development',
			user: DB_USER,
			password: DB_PASSWORD
		},
		migrations: {
			directory: migrationsDir,
			tableName: 'knex_migrations'
		}
	},
	testing: {
		client: 'pg',
		connection: {
			host: DB_HOST,
			database: DB_DATABASE + '_test',
			user: DB_USER,
			password: DB_PASSWORD
		},
		migrations: {
			directory: migrationsDir,
			tableName: 'knex_migrations'
		}
	},
	production: {
		client: 'pg',
		connection: {
			host: DB_HOST,
			database: DB_DATABASE,
			user: DB_USER,
			password: DB_PASSWORD
		},
		migrations: {
			directory: migrationsDir,
			tableName: 'knex_migrations'
		}
	}
};
