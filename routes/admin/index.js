const express = require('express');
const router = express.Router();
const users = require('./users');
const articles = require('./articles');
const cottages = require('./cottages');
const areas = require('./areas');

router.use('/', users);
router.use('/', articles);
router.use('/', cottages);
router.use('/', areas);

module.exports = router;
