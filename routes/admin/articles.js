const express = require('express');
const router = express.Router();
const { Article, LinkBlock, Navigation } = require('../../models');
const { isLoggedIn } = require('../../utils/middleware');
const { kebabCaseToCamelcaseObject, camelCaseToKebabCaseObject } = require('../../utils');
const validate = require('../../utils/validate');

router.get('/articles', isLoggedIn, (req, res) => {
	const userId = req.session.user.id;

	Article
		.find({ userId })
		.then(articles => {
			res.render('admin/articles/article_list.html', { articles });
		}).catch(error => {
			console.log(error);
		});
});

router.get('/articles/post', isLoggedIn, (req, res, next) => {
	res.render('admin/articles/article_form.html', {
		action: 'create'
	});
});

router.post('/articles/post', isLoggedIn, (req, res, next) => {
	const body = req.body;
	const user = req.session.user;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);

	const article = kebabCaseToCamelcaseObject(body);
	let linkBlocks = Array.prototype.slice.call(body['link-blocks']);
	let navigations = body.navigations;

	article.userId = user.id;
	article.isPublished = article.isPublished !== undefined;
	delete article.linkBlocks;
	delete article.navigations;

	// article.metaRobots = article.metaRobots === '' ? 'index,robots' : article.metaRobots;

	const mainTitleErrors = validate(camelCaseBody.mainTitle).notEmpty();
	const urlErrors = validate(body.url).notEmpty();

	let errors = { general: [] };
	if (!mainTitleErrors.isValid() || !urlErrors.isValid()) {
		errors.mainTitle = Object.values(mainTitleErrors.errors);
		errors.url = Object.values(urlErrors.errors);
		return res.render('admin/articles/article_form.html', {
			action: 'create',
			body,
			errors
		});
	}

	Article
		.insert(article)
		.then(([article]) => {
			linkBlocks = linkBlocks
				.filter(block => block['link-href'] !== '')
				.map(block => {
					const camelCaseBlock = kebabCaseToCamelcaseObject(block);
					camelCaseBlock.articleId = article.id;
					return camelCaseBlock;
				});

			return Promise.all([
				LinkBlock.insert(linkBlocks),
				article
			]);
		}).then(([linkBlocks, article]) => {
			navigations.top.isTop = true;
			navigations.bottom.isTop = false;
			navigations.top.articleId = article.id;
			navigations.bottom.articleId = article.id;
			return Promise.resolve([article]);
		}).then(([article]) => {
			const promises = [ article, null, null ];
			const validTopLinks = navigations.top.links.filter(link => link.href !== '');
			const topNavigationData = Object.assign({}, navigations.top);
			delete topNavigationData.links;
			if (navigations.top.title !== '' || validTopLinks.length > 0)
				promises[1] = Navigation.insert(topNavigationData);

			const validBottomLinks = navigations.bottom.links.filter(link => link.href !== '');
			const bottomNavigationData = Object.assign({}, navigations.bottom);
			delete bottomNavigationData.links;
			if (navigations.bottom.title !== '' || validBottomLinks.length > 0)
				promises[2] = Navigation.insert(bottomNavigationData);
			return Promise.all(promises);
		}).then(([article, topNavigations, bottomNavigations]) => {
			const promises = [ article, null, null ];

			if (topNavigations) {
				const [topNavigation] = topNavigations;
				navigations.top.links.forEach(link => {
					link.navigationId = topNavigation.id;
				});
				promises[1] = Navigation.insertLink(navigations.top.links);
			}

			if (bottomNavigations) {
				const [bottomNavigation] = bottomNavigations;
				navigations.bottom.links.forEach(link => {
					link.navigationId = bottomNavigation.id;
				});
				promises[2] = Navigation.insertLink(navigations.bottom.links);
			}
			return Promise.all(promises);
		}).then(([article]) => {
			res.redirect('/admin/dashboard');
		}).catch(error => {
			console.log(error);
			res.render('admin/articles/article_form.html', {
				action: 'create',
				body,
				errors: { general: [ 'Please complete form' ] }
			});
		});
});

router.get('/articles/:id/update', isLoggedIn, (req, res) => {
	// if not yours and you are not admin you can't
	const { id } = req.params;

	Article
		.findOne({ id }, '*', true)
		.then(article => {
			if (req.session.user.id !== article.userId)
				return res.render('admin/articles/article_form.html', {
					notAllowed: true
				});

			article = camelCaseToKebabCaseObject(article);
			article['link-blocks'].forEach((link, index, links) => {
				links[index] = camelCaseToKebabCaseObject(link);
			});

			res.render('admin/articles/article_form.html', {
				action: 'update',
				body: article
			});
		}).catch(error => {
			console.log(error);
			res.render('404.html');
		});
});

router.post('/articles/:id/update', isLoggedIn, (req, res) => {
	const articleId = req.params.id;
	const body = req.body;
	const user = req.session.user;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);

	const article = kebabCaseToCamelcaseObject(body);
	let linkBlocks = Array.prototype.slice.call(body['link-blocks']);
	let navigations = body.navigations;

	article.userId = user.id;
	article.isPublished = article.isPublished !== undefined;
	delete article.linkBlocks;
	delete article.navigations;

	const mainTitleErrors = validate(camelCaseBody.mainTitle).notEmpty();
	const urlErrors = validate(body.url).notEmpty();

	let errors = { general: [] };
	if (!mainTitleErrors.isValid() || !urlErrors.isValid()) {
		errors.mainTitle = Object.values(mainTitleErrors.errors);
		errors.url = Object.values(urlErrors.errors);
		return res.render('admin/articles/article_form.html', {
			action: 'create',
			body,
			errors
		});
	}

	Promise.all([
		LinkBlock.remove({ articleId }),
		Navigation.remove({ articleId })
	]).then(result => {
		return Article.update(article, { id: articleId });
	}).then(([article]) => {
		linkBlocks = linkBlocks
			.filter(block => block['link-href'] !== '')
			.map(block => {
				const camelCaseBlock = kebabCaseToCamelcaseObject(block);
				camelCaseBlock.articleId = article.id;
				return camelCaseBlock;
			});

		return Promise.all([
			LinkBlock.insert(linkBlocks),
			article
		]);
	}).then(([linkBlocks, article]) => {
		navigations.top.isTop = true;
		navigations.bottom.isTop = false;
		navigations.top.articleId = article.id;
		navigations.bottom.articleId = article.id;
		return Promise.resolve([article]);
	}).then(([article]) => {
		const promises = [ article, null, null ];

		const validTopLinks = navigations.top.links.filter(link => link.href !== '');
		const topNavigationData = Object.assign({}, navigations.top);
		delete topNavigationData.links;
		if (navigations.top.title !== '' || validTopLinks.length > 0)
			promises[1] = Navigation.insert(topNavigationData);

		const validBottomLinks = navigations.bottom.links.filter(link => link.href !== '');
		const bottomNavigationData = Object.assign({}, navigations.bottom);
		delete bottomNavigationData.links;
		if (navigations.bottom.title !== '' || validBottomLinks.length > 0)
			promises[2] = Navigation.insert(bottomNavigationData);

		return Promise.all(promises);
	}).then(([article, topNavigations, bottomNavigations]) => {
		const promises = [ article, null, null ];

		if (topNavigations) {
			const [topNavigation] = topNavigations;
			navigations.top.links.forEach(link => {
				link.navigationId = topNavigation.id;
			});
			promises[1] = Navigation.insertLink(navigations.top.links);
		}

		if (bottomNavigations) {
			const [bottomNavigation] = bottomNavigations;
			navigations.bottom.links.forEach(link => {
				link.navigationId = bottomNavigation.id;
			});
			promises[2] = Navigation.insertLink(navigations.bottom.links);
		}
		return Promise.all(promises);
	}).then(([article]) => {
		res.redirect('/admin/dashboard');
	}).catch(error => {
		console.log(error);
		res.render('admin/articles/article_form.html', {
			action: 'update',
			body,
			errors: { general: [ 'Please complete form' ] }
		});
	});
});

router.post('/articles/:id/delete', isLoggedIn, (req, res, next) => {
	const userId = req.session.user.id;
	const articleId = req.params.id;

	Article
		.remove({ id: articleId })
		.then(result => {
			if (result.length !== 1)
				return Promise.reject('Article was not deleted properly');
			res.redirect('/admin/articles');
		}).catch(error => {
			console.log(errors);
			next(error);
		});
});

router.get('/test', (req, res, next) => {
	Article
		.findOne({ id: 4 }, '*', true)
		.then(article => {
			console.log(article)
			res.render('test.html', { article });
		}).catch(error => {
			next(error);
		});
});

module.exports = router;
