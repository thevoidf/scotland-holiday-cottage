const express = require('express');
const router = express.Router();
const { Area } = require('../../models');
const { isLoggedIn } = require('../../utils/middleware');
const validate = require('../../utils/validate');

router.get('/areas/post', isLoggedIn, (req, res, next) => {
	res.render('admin/cottages/area_form.html', {
		action: 'create'
	});
});

router.post('/areas/post', isLoggedIn, async (req, res, next) => {
	const body = req.body;

	const areaErrors = validate(body.name).notEmpty({ message: 'Area is required' });
	let errors = { general: [] };

	if (!areaErrors.isValid()) {
		errors.area = Object.values(areaErrors.errors);
		return res.render('admin/cottages/area_form.html', {
			action: 'create',
			body,
			errors
		});
	}

	const [area] = await Area.insertArea({ name: body.name, slug: body.slug });
	for (const countyData of body.counties) {
		if (countyData.name === '')
			continue;

		const [county] = await Area.insertCounty({
			name: countyData.name,
			slug: countyData.slug,
			areaId: area.id
		});

		for (const town of countyData.towns) {
			if (town.name === '')
				continue;
			await Area.insertTown({
				name: town.name,
				slug: town.slug,
				countyId: county.id
			});
		}
	}

	res.redirect('/admin/areas');
});

router.get('/areas/:id/update', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;

	const area = await Area.findOneArea({ id });
	const counties = await Area.findCounty({ areaId: area.id });

	area.counties = counties;

	for (const county of area.counties) {
		const towns = await Area.findTown({ countyId: county.id });
		county.towns = towns;
	}

	res.render('admin/cottages/area_form.html', {
		action: 'update',
		body: area
	});
});

router.post('/areas/:id/update', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	const body = req.body;

	const areaErrors = validate(body.name).notEmpty({ message: 'Area is required' });
	let errors = { general: [] };

	if (!areaErrors.isValid()) {
		errors.area = Object.values(areaErrors.errors);
		return res.render('admin/cottages/area_form.html', {
			action: 'update',
			body,
			errors
		});
	}

	const [area] = await Area.updateArea({
		name: body.name, slug: body.slug
	}, { id });

	const [county] = await Area.removeCounty({ areaId: area.id });

	for (const countyData of body.counties) {
		if (countyData.name === '')
			continue;

		const [county] = await Area.insertCounty({
			name: countyData.name,
			slug: countyData.slug,
			areaId: area.id
		});

		for (const town of countyData.towns) {
			if (town.name === '')
				continue;

			await Area.insertTown({
				name: town.name,
				slug: town.slug,
				countyId: county.id
			});
		}
	}

	res.redirect('/admin/areas');
});

router.post('/areas/:id/delete', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	await Area.removeArea({ id });
	res.redirect('/admin/areas');
});

router.get('/areas', async (req, res, next) => {
	const areas = await Area.findAllArea();
	res.render('admin/cottages/area_list.html', { areas });
});

router.get('/areas.json', (req, res, next) => {
	Area
		.findAllArea()
		.then(areas => {
			res.json(areas);
		});
});

router.get('/areas/:name/counties', (req, res, next) => {
	const { name } = req.params;

	Area
		.findOneArea({ name })
		.then(area => {
			return Area.findCounty({ areaId: area.id });
		}).then(counties => {
			res.json(counties);
		});
});

router.get('/areas/:areaName/counties/:countyName', (req, res, next) => {
	const { areaName, countyName } = req.params;
	Area
		.findOneCounty({ name: countyName })
		.then(county => {
			return Area.findTown({ countyId: county.id });
		}).then(towns => {
			res.json(towns);
		});
});

module.exports = router;
