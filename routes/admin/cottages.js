const express = require('express');
const router = express.Router();
const { knex, User, Cottage, Area, CottageList, FilterProperty, SpecialOffer } = require('../../models');
const { isLoggedIn } = require('../../utils/middleware');
const { kebabCaseToCamelcaseObject, camelCaseToKebabCaseObject } = require('../../utils');
const validate = require('../../utils/validate');

router.get('/cottages', isLoggedIn, (req, res, next) => {
	const userId = req.session.user.id;

	Cottage
		.find({ userId })
		.then(cottages => {
			res.render('admin/cottages/cottage_list.html', { cottages });
		}).catch(error => {
			next(error);
		});
});

router.get('/cottages/post', isLoggedIn, async (req, res, next) => {
	const filters = await FilterProperty.findAll();
	res.render('admin/cottages/cottage_form.html', {
		action: 'create',
		filters
	});
});

router.post('/cottages/post', isLoggedIn, async (req, res, next) => {
	const body = req.body;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);
	const filters = await FilterProperty.findAll();

	camelCaseBody.userId = req.session.user.id;
	camelCaseBody.locationLat = parseFloat(camelCaseBody.locationLat) || 0;
	camelCaseBody.locationLng = parseFloat(camelCaseBody.locationLng) || 0;

	camelCaseBody.isAdvertisable = camelCaseBody.isAdvertisable !== undefined;
	camelCaseBody.disabledAccess = camelCaseBody.disabledAccess !== undefined;
	camelCaseBody.isPublished = camelCaseBody.isPublished !== undefined;

	const areaErrors = validate(camelCaseBody.area)
		.isNotEqual({ value: 'empty', message: 'Area is required' });
	const countyErrors = validate(body.county)
		.isNotEqual({ value: 'empty', message: 'County is required' });
	const townErrors = validate(body.town)
		.isNotEqual({ value: 'empty', message: 'Town is required' });
	const slugErrors = validate(body.slug).notEmpty({ message: 'Slug is required' });
	const listingTitleErrors = validate(camelCaseBody.listingTitle).notEmpty({ message: 'Listing title is required' });

	let errors = { general: [] };

	if (!areaErrors.isValid() || !countyErrors.isValid() ||
			!slugErrors.isValid() || !listingTitleErrors.isValid()) {
		errors.area = Object.values(areaErrors.errors);
		errors.county = Object.values(countyErrors.errors);
		errors.town = Object.values(townErrors.errors);
		errors.slug = Object.values(slugErrors.errors);
		errors.listingTitle = Object.values(listingTitleErrors.errors);
		return res.render('admin/cottages/cottage_form.html', {
			action: 'create',
			filters,
			body,
			errors
		});
	}

	const area = await Area.findOneArea({ name: body.area });
	const county = await Area.findOneCounty({ name: body.county });
	const town = await Area.findOneTown({ name: body.town });

	delete camelCaseBody.area;
	delete camelCaseBody.county;
	delete camelCaseBody.town;
	delete camelCaseBody.filters;

	camelCaseBody.areaId = area.id;
	camelCaseBody.countyId = county.id;
	camelCaseBody.townId = town.id;

	const [createdCottage] = await Cottage.insert(camelCaseBody);

	if (body.filters) {
		for (const filter of body.filters) {
			const [foundFilter] = await FilterProperty.find({ title: filter });
			await FilterProperty.insertCottageFilter({
				cottageId: createdCottage.id,
				filterId: foundFilter.id
			});
		}
	}

	res.redirect('/admin/cottages');
});

router.get('/cottages/:id/update', isLoggedIn, async (req, res, next) => {
	const body = req.body;
	const { id } = req.params;
	const allFilters = await FilterProperty.findAll();
	let selectedFilters = await FilterProperty.findCottageFilter({ cottageId: id });
	let selectedFilterTitles = [];

	for (const cottageFilter of selectedFilters) {
		const [filter] = await FilterProperty.find({ id: cottageFilter.filterId });
		selectedFilterTitles.push(filter.title)
	}

	Cottage
		.findOne({ id })
		.then(cottage => {
			if (!cottage)
				return res.render('404.html');
			if (req.session.user.id !== cottage.userId)
				return res.render('admin/cottage_form.html', {
					notAllowed: true
				});

			return Promise.all([
				cottage,
				Area.findOneArea({ id: cottage.areaId }),
				Area.findOneCounty({ id: cottage.countyId }),
				Area.findOneTown({ id: cottage.townId }),
			]);
		}).then(([cottage, area, county, town]) => {
			const cottageBody = camelCaseToKebabCaseObject(cottage);
			cottageBody.area = area.name;
			cottageBody.county = county.name;
			cottageBody.town = town.name;
			cottageBody.filters = selectedFilterTitles;

			console.log(cottageBody)

			res.render('admin/cottages/cottage_form.html', {
				action: 'update',
				filters: allFilters,
				body: cottageBody
			});
		}).catch(error => {
			console.log(error);
		});
});

router.post('/cottages/:id/update', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	const body = req.body;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);
	const filters = await FilterProperty.findAll();

	camelCaseBody.userId = req.session.user.id;
	camelCaseBody.locationLat = parseFloat(camelCaseBody.locationLat) || 0;
	camelCaseBody.locationLng = parseFloat(camelCaseBody.locationLng) || 0;

	camelCaseBody.isAdvertisable = camelCaseBody.isAdvertisable !== undefined;
	camelCaseBody.disabledAccess = camelCaseBody.disabledAccess !== undefined;
	camelCaseBody.isPublished = camelCaseBody.isPublished !== undefined;

	const areaErrors = validate(camelCaseBody.area)
		.isNotEqual({ value: 'empty', message: 'Area is required' });
	const countyErrors = validate(body.county)
		.isNotEqual({ value: 'empty', message: 'County is required' });
	const townErrors = validate(body.town)
		.isNotEqual({ value: 'empty', message: 'Town is required' });
	const slugErrors = validate(body.slug).notEmpty({ message: 'Slug is required' });
	const listingTitleErrors = validate(camelCaseBody.listingTitle).notEmpty({ message: 'Listing title is required' });

	let errors = { general: [] };

	if (!areaErrors.isValid() || !countyErrors.isValid() ||
		!slugErrors.isValid() || !listingTitleErrors.isValid()) {
		errors.area = Object.values(areaErrors.errors);
		errors.county = Object.values(countyErrors.errors);
		errors.town = Object.values(townErrors.errors);
		errors.slug = Object.values(slugErrors.errors);
		errors.listingTitle = Object.values(listingTitleErrors.errors);
		return res.render('admin/cottages/cottage_form.html', {
			action: 'create',
			filters,
			body,
			errors
		});
	}

	const area = await Area.findOneArea({ name: body.area });
	const county = await Area.findOneCounty({ name: body.county });
	const town = await Area.findOneTown({ name: body.town });

	delete camelCaseBody.area;
	delete camelCaseBody.county;
	delete camelCaseBody.town;
	delete camelCaseBody.filters;

	camelCaseBody.areaId = area.id;
	camelCaseBody.countyId = county.id;
	camelCaseBody.townId = town.id;

	const [createdCottage] = await Cottage.update(camelCaseBody, { id });

	await FilterProperty.removeCottageFilter({ cottageId: createdCottage.id });
	if (body.filters) {
		for (const filter of body.filters) {
			const [foundFilter] = await FilterProperty.find({ title: filter });
			await FilterProperty.insertCottageFilter({
				cottageId: createdCottage.id,
				filterId: foundFilter.id
			});
		}
	}

	res.redirect('/admin/cottages');
});

router.get('/cottages/search.json', isLoggedIn, async (req, res, next) => {
	const { slug } = req.query;
	const cottages = await knex('cottages')
		.select(['slug'])
		.where('slug', 'ilike', `%${slug}%`);
	res.json(cottages);
});

router.post('/cottages/:id/delete', isLoggedIn, (req, res, next) => {
	const userId = req.session.user.id;
	const cottageId = req.params.id;

	Cottage
		.findOne({ userId, }, ['userId'])
		.then(cottage => {
			if (req.session.user.id !== cottage.userId)
				return Promise.reject('permission denied');
			if (!cottage)
				return Promise.reject('cottage not found');
			return Cottage.remove({ id: cottageId });
		}).then(result => {
			if (result.length !== 1)
				return res.send('Cottage was not removed properly');
			res.redirect('/admin/cottages');
		}).catch(error => {
			res.status(500).send(error);
		});
});

router.get('/cottage-lists/post', isLoggedIn, (req, res, next) => {
	res.render('admin/cottages/cottage_list_form.html', {
		action: 'create'
	});
});


router.get('/cottage-lists', isLoggedIn, async (req, res, next) => {
	const cottageLists = await CottageList.findAll();
	res.render('admin/cottages/cottage_list_list.html', { cottageLists });
});

router.post('/cottage-lists/post', isLoggedIn, async (req, res, next) => {
	const body = req.body;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);

	let cottageList = camelCaseBody;
	let topLinks = camelCaseBody.links;
	let cottages = camelCaseBody.cottages;
	let linkBlocks = camelCaseBody.linkBlocks
		.map(block => kebabCaseToCamelcaseObject(block));

	camelCaseBody.isPublished = camelCaseBody.isPublished !== undefined;
	delete camelCaseBody.links;
	delete camelCaseBody.cottages;
	delete camelCaseBody.linkBlocks;

	let errors = { general: [] };
	const mainTitleErrors = validate(camelCaseBody.mainTitle).notEmpty({
		message: 'Main title is required'
	});
	const slugErrors = validate(camelCaseBody.slug).notEmpty({
		message: 'Slug is required'
	});
	if (!mainTitleErrors.isValid() || !slugErrors.isValid()) {
		errors.mainTitle = Object.values(mainTitleErrors.errors);
		errors.slug = Object.values(slugErrors.errors);
		return res.render('admin/cottages/cottage_list_form.html', {
			action: 'create',
			body,
			errors
		});
	}

	const [createdCottageList] = await CottageList.insert(cottageList);

	if (topLinks.left.href !== '') {
		topLinks.left.cottageListId = createdCottageList.id;
		topLinks.left.isLeft = true;
		await CottageList.insertLink(topLinks.left);
	}
	if (topLinks.right.href !== '') {
		topLinks.right.cottageListId = createdCottageList.id;
		await CottageList.insertLink(topLinks.right);
	}

	let validCottages = [];
	for (const cottage of cottages) {
		const actualCottage = await Cottage.findOne({ slug: cottage.slug });
		if (actualCottage === undefined)
			continue;

		cottage.cottageListId = createdCottageList.id;
		cottage.cottageId = actualCottage.id;
		delete cottage.slug;

		validCottages.push(cottage);
	}

	linkBlocks = linkBlocks
		.filter(block => block.linkHref !== '')
		.map(block => {
			block.cottageListId = createdCottageList.id;
			return block;
		});

	await CottageList.insertCottage(validCottages);
	await CottageList.insertBlock(linkBlocks);

	res.redirect('/admin/cottage-lists');
});

router.get('/cottage-lists/:id/update', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	let cottageList = await CottageList.findOne({ id });
	const [leftLink] = await CottageList.findLink({ cottageListId: id, isLeft: true });
	const [rightLink] = await CottageList.findLink({ cottageListId: id, isLeft: false });
	let cottages = await CottageList.findCottage({ cottageListId: id });
	let linkBlocks = await CottageList.findBlock({ cottageListId: id });
	linkBlocks = linkBlocks.map(block => camelCaseToKebabCaseObject(block));

	for (const cottage of cottages) {
		const actualCottage = await Cottage.findOne({ id: cottage.cottageId });
		cottage.slug = actualCottage.slug;
	}

	cottageList.links = {};

	cottageList.links.left = leftLink;
	cottageList.links.right = rightLink;
	cottageList.cottages = cottages;
	cottageList.linkBlocks = linkBlocks;
	cottageList = camelCaseToKebabCaseObject(cottageList);

	res.render('admin/cottages/cottage_list_form.html', {
		action: 'update',
		body: cottageList
	});
});

router.post('/cottage-lists/:id/update', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	const body = req.body;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);

	let cottageList = camelCaseBody;
	let topLinks = camelCaseBody.links;
	let cottages = camelCaseBody.cottages;
	let linkBlocks = camelCaseBody.linkBlocks
		.map(block => kebabCaseToCamelcaseObject(block));

	camelCaseBody.isPublished = camelCaseBody.isPublished !== undefined;
	delete camelCaseBody.links;
	delete camelCaseBody.cottages;
	delete camelCaseBody.linkBlocks;

	let errors = { general: [] };
	const mainTitleErrors = validate(camelCaseBody.mainTitle).notEmpty({
		message: 'Main title is required'
	});
	const slugErrors = validate(camelCaseBody.slug).notEmpty({
		message: 'Slug is required'
	});
	if (!mainTitleErrors.isValid() || !slugErrors.isValid()) {
		errors.mainTitle = Object.values(mainTitleErrors.errors);
		errors.slug = Object.values(slugErrors.errors);
		return res.render('admin/cottages/cottage_list_form.html', {
			action: 'create',
			body,
			errors
		});
	}

	await CottageList.removeLink({ cottageListId: id });
	await CottageList.removeCottage({ cottageListId: id });
	await CottageList.removeBlock({ cottageListId: id });

	const [createdCottageList] = await CottageList.update(cottageList, { id });

	if (topLinks.left.href !== '') {
		topLinks.left.cottageListId = createdCottageList.id;
		topLinks.left.isLeft = true;
		await CottageList.insertLink(topLinks.left);
	}
	if (topLinks.right.href !== '') {
		topLinks.right.cottageListId = createdCottageList.id;
		await CottageList.insertLink(topLinks.right);
	}

	let validCottages = [];
	for (const cottage of cottages) {
		const actualCottage = await Cottage.findOne({ slug: cottage.slug });
		if (actualCottage === undefined)
			continue;

		cottage.cottageListId = createdCottageList.id;
		cottage.cottageId = actualCottage.id;
		delete cottage.slug;

		validCottages.push(cottage);
	}

	linkBlocks = linkBlocks
		.filter(block => block.linkHref !== '')
		.map(block => {
			block.cottageListId = createdCottageList.id;
			return block;
		});

	await CottageList.insertCottage(validCottages);
	await CottageList.insertBlock(linkBlocks);

	res.redirect('/admin/cottage-lists');
});

router.post('/cottage-lists/:id/delete', isLoggedIn, async (req, res, next) => {
	await CottageList.remove({ id: req.params.id });
	res.redirect('/admin/cottage-lists');
});

router.get('/filter-properties', isLoggedIn, async (req, res, next) => {
	const existingFilters = await FilterProperty.findAll();
	if (existingFilters.length !== 0) {
		return res.render('admin/cottages/filter_form.html', {
			filters: existingFilters,
			action: 'update'
		});
	}

	res.render('admin/cottages/filter_form.html', { action: 'create' });
});

router.post('/filter-properties', isLoggedIn, async (req, res, next) => {
	const body = req.body;
	const oldFilters = await FilterProperty.findAll().map(filter => filter.title);
	const newFilters = body.filters.split(',').filter(filter => filter !== '');

	const allFilters = Array.from(new Set(oldFilters.concat(newFilters)))
	const removedFilters = allFilters.filter(filter => oldFilters.includes(filter) && !newFilters.includes(filter))
	const addedFilters = allFilters.filter(filter => !oldFilters.includes(filter) && newFilters.includes(filter))

	for (const removed of removedFilters) {
		await FilterProperty.remove({ title: removed });
	}
	for (const added of addedFilters) {
		await FilterProperty.insert({ title: added });
	}

	res.redirect('/admin/filter-properties');
});

router.get('/special-offers/post', isLoggedIn, async (req, res, next) => {
	const cottages = await Cottage
		.find({ userId: req.session.user.id }, ['slug'])
		.map(cottage => cottage.slug);

	res.render('admin/cottages/special_offer_form.html', { cottages });
});

router.post('/special-offers/post', isLoggedIn, async (req, res, next) => {
	const body = req.body;
	const camelCaseBody = kebabCaseToCamelcaseObject(body);
	const cottages = await Cottage
		.find({ userId: req.session.user.id }, ['slug'])
		.map(cottage => cottage.slug);

	const cottageErrors = validate(body.cottage)
		.isNotEqual({ value: 'empty', message: 'Cottage is required' });
	const offerErrors = validate(body.offer).notEmpty({ message: 'Offer is required' });
	const startDateErrors = validate(camelCaseBody.startDate).notEmpty({ message: 'Start date is required' });
	const endDateErrors = validate(camelCaseBody.endDate).notEmpty({ message: 'End date is required' });

	let errors = { general: [] };

	if (!cottageErrors.isValid() || !offerErrors.isValid() ||
			!startDateErrors.isValid() || !endDateErrors.isValid()) {
		errors.cottage = Object.values(cottageErrors.errors);
		errors.offer = Object.values(offerErrors.errors);
		errors.startDate = Object.values(startDateErrors.errors);
		errors.endDate = Object.values(endDateErrors.errors);
		return res.render('admin/cottages/special_offer_form.html', {
			body, errors, cottages
		});
	}

	const foundCottage = await Cottage.findOne({ slug: camelCaseBody.cottage });
	const offer = camelCaseBody;
	delete offer.cottage;
	offer.cottageId = foundCottage.id;

	await SpecialOffer.insert(offer);

	res.redirect('/admin/special-offers');
});

router.get('/special-offers', isLoggedIn, async (req, res, next) => {
	const cottageIds = await Cottage
		.find({ userId: req.session.user.id }, ['id'])
		.map(cottage => cottage.id);
	const offers = await knex
		.select('*')
		.from('special_offers')
		.whereIn('cottage_id', cottageIds);

	console.log(offers)

	return res.render('admin/cottages/special_offer_list.html', { offers });
});

router.post('/special-offers/:id/delete', isLoggedIn, async (req, res, next) => {
	const { id } = req.params;
	await SpecialOffer.remove({ id });
	res.redirect('/admin/special-offers');
});

module.exports = router;
