const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { User } = require('../../models');
const { isLoggedIn } = require('../../utils/middleware');
const validate = require('../../utils/validate');

router.get('/signup', (req, res) => {
	res.render('admin/users/signup.html');
});

router.post('/signup', (req, res) => {
	const body = req.body;
	const { username, email, password } = body;
	const saltRounds = 10;
	const usernameErrors = validate(username)
		.notEmpty({ message: 'Username is required' })
		.isLength({ min: 3, message: 'Username is too short' });
	const emailErrors = validate(email)
		.notEmpty({ message: 'Email is required' })
		.isEmail({ message: 'Invalid email' });
	const passwordErrors = validate(password)
		.notEmpty({ message: 'Password is required' })
		.isLength({ min: 6, message: 'Password is too short' });
	const isUserValid = usernameErrors.isValid() &&
		emailErrors.isValid() &&
		passwordErrors.isValid();
	let errors = { general: [] };

	if (!isUserValid) {
		errors.username = Object.values(usernameErrors.errors);
		errors.email = Object.values(emailErrors.errors);
		errors.password = Object.values(passwordErrors.errors);
		return res.render('admin/users/signup', { errors, body });
	}

	Promise.all([
		User.findOne({ username }),
		User.findOne({ email })
	]).then(([usernameUser, emailUser]) => {
		if (usernameUser) {
			errors.general.push('Username already registered');
			return Promise.reject(errors);
		}
		if (emailUser) {
			errors.general.push('Email already registered');
			return Promise.reject(errors);
		}
		return bcrypt.hash(password, saltRounds);
	}).then(hash => {
		return User.insert({
			username,
			email,
			password: hash
		});
	}).then(([user]) => {
		req.session.user = {
			id: user.id,
			username: user.username
		};
		res.redirect('/admin/dashboard');
	}).catch(errors => {
		res.render('admin/users/signup', { errors, body });
	});
});

router.get('/', (req, res) => {
	if (req.session.user)
		return res.redirect('/admin/dashboard');
	res.render('admin/users/login');
});

router.post('/', (req, res) => {
	const body = req.body;
	const { email, password } = body;
	const emailErrors = validate(email)
		.notEmpty({ message: 'Email is required' })
		.isEmail({ message: 'Invalid email address' });
	const passwordErrors = validate(password)
		.notEmpty({ message: 'Password is required' })
		.isLength({ min: 6, message: 'Password is too short' });
	let errors = {};

	if (!(emailErrors.isValid() && passwordErrors.isValid())) {
		errors.email = Object.values(emailErrors.errors);
		errors.password = Object.values(passwordErrors.errors);
		return res.render('admin/users/login', { errors, body });
	}

	User
		.findOne({ email })
		.then(user => {
			if (!user)
				return Promise.reject({ 
					general: [ 'Incorrect email or password' ]
				});
			return Promise.all([
				user,
				bcrypt.compare(password, user.password)
			]);
		}).then(([user, isPasswordCorrect]) => {
			if (!isPasswordCorrect)
				return Promise.reject({ 
					general: [ 'Incorrect email or password' ]
				});
			req.session.user = {
				id: user.id,
				username: user.username
			};
			res.redirect('/admin/dashboard');
		}).catch(errors => {
			res.render('admin/users/login', { errors, body });
		});
});

router.get('/dashboard', isLoggedIn, (req, res) => {
	const userId = req.session.user.id;
	res.render('admin/users/dashboard');
});

router.get('/logout', (req, res, next) => {
	req.session.destroy(error => {
		if (error) next(error);
		res.redirect('/');
	});
});

module.exports = router;
