const express = require('express');
const router = express.Router();
const { Cottage } = require('../models');

router.get('/', (req, res) => {
	Cottage
		.findAll()
		.then(cottages => {
			res.render('index');
		});
});

module.exports = router;
