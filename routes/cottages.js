const express = require('express');
const router = express.Router();
const { knex, Cottage } = require('../models');

router.get('/', (req, res) => {
	Cottage
		.findAll()
		.then(cottages => {
			res.render('cottages/list', { cottages });
		});
});

router.get('/:slug', (req, res) => {
	const { slug } = req.params;

	knex('cottages')
		.where({ slug })
		.join('users', 'users.id', '=', 'cottages.user_id')
		.select('*')
		.then(([cottage]) => {
			delete cottage.password;
			res.render('cottages/detail', { cottage });
		});
});

module.exports = router;
