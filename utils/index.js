const fs = require('fs');
const moment = require('moment');

module.exports = {
	upperCase: str => {
		return str.charAt(0).toUpperCase() + str.substr(1);
	},
	toPascalCase: str => {
		return str.replace(/(\w)(\w*)/g, (g0, g1, g2) => {
			return g1.toUpperCase() + g2.toLowerCase();
		});
	},
	kebabCaseToCamelcase: str => {
		return str.replace(/-([a-z])/g, g => {
			return g[1].toUpperCase();
		});
	},
	camelCaseToKebabCase: str => {
		return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
	},
	kebabCaseToCamelcaseObject: object => {
		let camelCaseObject = {};
		Object.keys(object).map((key, index) => {
			camelCaseObject[module.exports.kebabCaseToCamelcase(key)] = object[key];
		});
		return camelCaseObject;
	},
	camelCaseToKebabCaseObject: object => {
		let kebabCaseObject = {};
		Object.keys(object).map((key, index) => {
			kebabCaseObject[module.exports.camelCaseToKebabCase(key)] = object[key];
		});
		return kebabCaseObject;
	},
	logError: log => {
		const logPath = './logs/error.log';
		const timestamp = moment().format('YYYY/MMM/DD/hh:mm A');
		log = `[${timestamp}] ${log}\n`;

		return new Promise((resolve, reject) => {
			fs.access(logPath, err => {
				if (err) {
					fs.writeFile(logPath, log, err => {
						if (err) return reject(err);
						resolve();
					});
				} else {
					fs.appendFile(logPath, log, err => {
						if (err) return reject(err);
						resolve();
					});
				}
			});
		});
	}
};
