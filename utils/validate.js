function validate(field) {
	if (!(this instanceof validate))
		return new validate(field);
	this.field = field;
	this.errors = {};
}

validate.prototype = {
	notEmpty: function(opts) {
		opts = opts || {};
		if (this.field === '')
			this.errors.notEmpty = opts.message || 'field is empty';
		return this;
	},
	isEqual: function(opts) {
		opts = opts || {};
		if (this.field !== opts.value)
			this.errors.isEqual = opts.message || `${this.field} is not equals to ${opts.value}`;
		return this;
	},
	isNotEqual: function(opts) {
		opts = opts || {};
		if (this.field === opts.value)
			this.errors.isNotEqual = opts.message || `${this.field} is equals to ${opts.value}`;
		return this;
	},
	isLength: function(opts) {
		const { min, max } = opts;
		if (this.field.length < min)
			this.errors.isLength = opts.message || opts.minMessage || 'field too short';
		if(this.field.length > max)
			this.errors.isLength = opts.message || opts.maxMessage || 'field too long';
		return this;
	},
	isEmail: function(opts) {
		opts = opts || {};
		if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.field))
			this.errors.isEmail = opts.message || 'invalid email';
		return this;
	},
	isOk: function(opts) {
		opts = opts || {};
		if (!this.field)
			this.errors.isOk = opts.message || 'field is not ok';
		return this;
	},
	getErrorList: function() {
		return Object.values(this.errors);
	},
	isValid: function() {
		return this.getErrorList().length == 0;
	}
};

module.exports = validate;
