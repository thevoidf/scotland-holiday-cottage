require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const redis = require('redis').createClient();
const RedisStore = require('connect-redis')(session);
const path = require('path');
const nunjucks = require('nunjucks');
const fs = require('fs');
const morgan = require('morgan');
const { logError } = require('./utils');

const app = express();

const { PORT = 3000, NODE_ENV = 'development' } = process.env;

if (NODE_ENV === 'development') {
	app.use('/static', express.static(path.join(__dirname, 'public')));
}

const accessLogStream = fs.createWriteStream(path.join(__dirname, './logs/access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
	store: new RedisStore({
		client: redis,
		host: 'localhost',
		port: 6379,
		ttl: 86400
	}),
	secret: process.env.SESSION_SECRET,
	resave: false,
	saveUninitialized: true
	// cookie: { secure: true }
}));
nunjucks.configure('views', {
	autoescape: true,
	watch: true,
	express: app
});
app.set('view engine', 'html');

const index = require('./routes');
const admin = require('./routes/admin');
const cottages = require('./routes/cottages');

app.use((req, res, next) => {
	if (req.session.user)
		res.locals.username = req.session.user.username;
	next();
});

app.use(index);
app.use('/admin', admin);
app.use('/cottages', cottages);

app.use((req, res, next) => {
	res.status(404).render('404.html');
});

app.use((err, req, res, next) => {
	logError(err.stack).then(() => {
		if (NODE_ENV === 'development' || NODE_ENV === 'testing')
			console.log(err.stack);
		res.status(500).render('error');
	});
});

app.listen(PORT, () => {
	console.log(`scotland on port ${PORT}`);
});

module.exports = app;
